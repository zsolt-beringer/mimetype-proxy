# mimetype-proxy

A simple proof of concept proxy that changes the returned mime-type

## TODO

* Security review
* Queue requests so that the upstream server is not overloaded in bursts
* Return more refined HTTP status codes on error

Pull requests welcome!

## LICENSE

* AGPLv3
  * https://www.gnu.org/licenses/agpl-3.0.en.html
